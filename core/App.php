<?php

class App
{

	private $table = 'posts';
    private $order = 'created_at';
    
    public function __construct(){ 
        $this->model = new PostModel();
        //$this->toLoad();
    }

    public function toLoad(){
        if (!isset($_GET['act']) ){
            $this->loadIndex($this->model,$this->table, $this->order);
        } else if ($_GET['act'] == 'add'){
            if (empty(validation())){
                $title = $_POST["title"];
                $body = $_POST["body"];
    
                $this->model->saveData($this->table,$_POST);
            }
            $this->loadIndex($this->model,$this->table, $this->order);
        } 
            
    }

    public function loadIndex($model,$table, $order){
        $datas = $model->showData($table, $order);
	    require_once('../views/post/index.php');
    }

}

?>