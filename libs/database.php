<?php

class Database
{

    private $instance = NULL;

    public function __construct() {
        $this->instance = $this->getInstance();
    }

    public function getInstance() {
        if (!isset($this->instance)) {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $this->instance = new PDO('mysql:host='. serverName .';dbname='. databaseName .'', ''. username .'', ''. password .'', $pdo_options);
        }
        return $this->instance;
    }

    public function insert($table,$post){
        $this->instance->query("INSERT INTO " . $table . " VALUES('', '$post[title]', '$post[body]', CURRENT_TIMESTAMP)");

    }

    public function display($table,$order){
        return $this->instance->query("SELECT * FROM " . " " . $table . " " . "ORDER BY" . " " . $order . " " . "DESC");
    }
}
?>