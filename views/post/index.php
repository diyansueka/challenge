
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="form-container">
                <div class="message">
                    <br> <?php echo validation(); ?>
                </div>
                <form method="post" action="/first-challenge/public/?act=add">
                    <div>
                        <label for="title">Title : </label>
                        <br>
                        <input type="text" id="title" name="title">
                    </div>
                    <div>
                        <label for="body">Body : </label>
                        <br>
                        <textarea name="body" rows="5" cols="40"></textarea>
                    </div>
                    <div>
                        <button type="submit" name="submit" class="button">Submit</button>
                    </div>
                </form>
            </div>

            <div class="posts">
                <?php foreach($datas as $data) : ?>
                    <hr>
                    <h3><?php echo $data['title'] ?></h3>
                    <p><?php echo $data["body"] ?></p>
                    <p class="post-created">
                        <?php echo date( "m-d-Y H:i", strtotime($data["created_at"])) ?>
                    </p>
                <?php endforeach; ?>
                <hr>
            </div>

        </div>
    </body>
</html>