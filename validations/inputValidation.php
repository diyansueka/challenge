<?php

    function validation(){
        
        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            return '';
        }
        $msg = '';
        if (empty($_POST["title"])) {
            $msg = $msg . 'Title must be filled in<br>';
        } else if (strlen($_POST["title"]) < 10 || strlen($_POST["title"]) > 32 ) {
            $msg = $msg . 'Your title must be 10 to 32 characters long<br>';
        }

        if (empty($_POST["body"])) {
            $msg = $msg . 'Message must be filled in<br>';
        } else if (strlen($_POST["body"]) < 10 || strlen($_POST["body"]) > 200 ) {
            $msg = $msg . 'Your message must be 10 to 200 characters long<br>';
        }

        return $msg;
    }

?>