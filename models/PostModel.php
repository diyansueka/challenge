<?php

class PostModel {

    public function __construct()
	{
	    $this->dataBase = new Database;
    }

    public function saveData($table,$post)
    {
	    $this->dataBase->insert($table,$post);
    }

    public function showData($table,$order)
    {
	    $result = $this->dataBase->display($table,$order);
	    return $result;
    }
}

?>